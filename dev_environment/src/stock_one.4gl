 GLOBALS "stock_glob.4gl" 

 FUNCTION one_menu() 
   CLEAR SCREEN 

  OPEN WINDOW w_stock AT 2,2 WITH FORM "stock"
     ATTRIBUTE(BORDER) 

  LET gr_stock.* = nr_stock.* 
  MENU "STOCK ONE" 

     BEFORE MENU 
       HIDE OPTION ALL 
       SHOW OPTION "Query", "Add", "Exit" 

     COMMAND "Query" "Query for a row in the stock table." 
       HELP 201 
       MESSAGE "" 
       CALL clean_up() 

       IF query_stock_ctl() THEN 
          SHOW OPTION "Next", "Previous", "Update", "Delete" 
       ELSE 
          HIDE OPTION ALL 
          SHOW OPTION "Query", "Add", "Exit" 
       END IF 

     COMMAND "Next" "Find the NEXT stock item from the list." 
       HELP 202 
       CALL fetch_rel_stock(1) 

     COMMAND "Previous" "Find the PREVIOUS stock item from the list." 
       HELP 203 
       CALL fetch_rel_stock(-1) 

     COMMAND "Update" "Change an existing stock item." 
       HELP 204 
       CALL update_stock() 

     COMMAND "Delete" "Delete an existing stock item." 
       HELP 205 
       CALL delete_stock() 

     COMMAND "Add" "Add a new stock item." 
       HELP 206 
       MESSAGE "" 
       CALL input_stock() 

     COMMAND "Exit" "Return to the STOCK MAIN menu." 
       HELP 901 
       CALL clean_up() 
       EXIT MENU 
  END MENU 

  CLOSE WINDOW w_stock 

END FUNCTION 


FUNCTION input_stock() 

  OPTIONS 
     MESSAGE LINE LAST - 2 

  DISPLAY "Press ACCEPT to add a row, INTERRUPT to cancel.", "" AT 1,1 
  DISPLAY "" At 2,1 

  LET int_flag = FALSE 

  INPUT BY NAME gr_stock.* 

     BEFORE INPUT 
       MESSAGE "Use CONTROL-W for field-level HELP." 

     BEFORE FIELD manu_code 
       MESSAGE "Press CONTROL-B for a list of ", "manufacturer codes." 

     ON KEY(CONTROL-B) 
       IF INFIELD(manu_code) THEN 
          CALL manufact_wind() RETURNING gr_stock.manu_code 
          IF int_flag = TRUE THEN 
             LET int_flag = FALSE 
             LET gr_stock.manu_code = NULL 
          ELSE 
             DISPLAY BY NAME gr_stock.manu_code 
             NEXT FIELD description 
          END IF 
       END IF 

     AFTER FIELD manu_code 
       MESSAGE "" 

       IF NOT have_manufact(gr_stock.manu_code) THEN 
          CALL manufact_wind() RETURNING gr_stock.manu_code 
          IF int_flag = TRUE THEN 
            LET int_flag = FALSE 
            LET gr_stock.manu_code = NULL 
            NEXT FIELD manu_code 
          ELSE 
            DISPLAY BY NAME gr_stock.manu_code 
            NEXT FIELD description 
          END IF 
       ELSE 
          IF pk_exists(gr_stock.stock_num, gr_stock.manu_code) THEN 
            ERROR "Stock Item already exists. ", "Please try again." 
            NEXT FIELD stock_num 
         END IF 
      END IF 

    ON KEY(CONTROL-W) 
       CASE 
          WHEN INFIELD(stock_num) 
             CALL SHOWHELP(301) 

          WHEN INFIELD(manu_code) 
             CALL SHOWHELP(302) 

          WHEN INFIELD(description) 
             CALL SHOWHELP(303) 

          WHEN INFIELD(unit_price) 
             CALL SHOWHELP(304) 

          WHEN INFIELD(unit) 
             CALL SHOWHELP(305) 

          WHEN INFIELD(unit_descr) 
             CALL SHOWHELP(306) 
        END CASE 
 END INPUT 


 IF int_flag = TRUE THEN 
    LET int_flag = FALSE 
    LET gr_stock.* = nr_stock.* 
    CLEAR FORM 
    ERROR "Data entry aborted." 
 ELSE 
    CALL insert_stock() 
 END IF 

END FUNCTION 


FUNCTION insert_stock() 

 DEFINE err_msg CHAR(80) 

    BEGIN WORK 

 WHENEVER ERROR CONTINUE 
    INSERT INTO stock VALUES(gr_stock.*) 
 WHENEVER ERROR STOP 

 IF SQLCA.SQLCODE < 0 THEN 
    LET err_msg = err_get(SQLCA.SQLCODE) 
      ROLLBACK WORK 
    ERROR err_msg 
    CALL errorlog(err_msg CLIPPED) 
 ELSE 
      COMMIT WORK 
      MESSAGE "The stock item has been added to the database." 
 END IF 

END FUNCTION 

FUNCTION have_manufact(mc) 
 DEFINE mc LIKE manufact.manu_code, 
         manu_count SMALLINT, 

         valid_manu_code SMALLINT 


 SELECT COUNT(*) INTO manu_count 
    FROM manufact 
    WHERE manu_code = mc 

 IF manu_count = 0 THEN 
    LET valid_manu_code = FALSE 
 ELSE 
    LET valid_manu_code = TRUE 
 END IF 
 RETURN valid_manu_code 
END FUNCTION 



FUNCTION pk_exists(st_num, mc) 
 DEFINE mc LIKE stock.manu_code, 
    st_num LIKE stock.stock_num, 
    pk_count SMALLINT, 
    valid_pk SMALLINT 

 SELECT COUNT(*) INTO pk_count 
    FROM stock 
    WHERE mc = manu_code AND st_num = stock_num 

 IF pk_count = 0 THEN 
    LET valid_pk = FALSE 
 ELSE 
    LET valid_pk = TRUE 
 END IF 

 RETURN valid_pk 
END FUNCTION 


FUNCTION manufact_wind() 

 DEFINE pa_elem SMALLINT 

 OPEN WINDOW w_manufact AT 3,40 
    WITH FORM "manu_wind" 
    ATTRIBUTE(BORDER) 

 DISPLAY "Press ACCEPT to select." AT 1,1 
 DISPLAY "Use Arrow keys to move." AT 2,1 

 CALL set_count(g_man_loaded) 
 LET int_flag = FALSE 

 DISPLAY ARRAY ga_manufact TO sa_manufact.* 
 CLOSE WINDOW w_manufact 

 LET pa_elem = arr_curr() 
 RETURN ga_manufact[pa_elem].manu_code 

END FUNCTION 
