database stores_demo
DEFINE arr_orders ARRAY[100] OF RECORD 
	customer_num like orders.customer_num,
	order_num like orders.order_num,
	order_date like orders.order_date,
	po_num like orders.po_num,
	ship_date like orders.ship_date,
	ship_charge like orders.ship_charge,
	paid_date like orders.paid_date
END RECORD
	
MAIN

	OPEN WINDOW w_orders AT 3,3 WITH FORM "orders_arr" 
	ATTRIBUTE ( BORDER ) 
	CALL display_orders()

END MAIN
FUNCTION display_orders () 
	DEFINE orders_cnt,arrcurr,scrline     SMALLINT, 
	i             SMALLINT, 
	cont_fetch    SMALLINT 
	DEFINE array_size SMALLINT

	DISPLAY "Press ACCEPT to continue, INTERRUPT to cancel." AT 1,1 
	DISPLAY "" AT 2,1 
	# Calculate array size
	SELECT count(*) INTO array_size
	FROM stock

	DECLARE crs_orders CURSOR FOR 
	SELECT orders.customer_num,
		orders.order_num,
		orders.order_date,
		orders.po_num,
		orders.ship_date	,
		orders.ship_charge	,
		orders.paid_date
	FROM orders 
	ORDER BY orders.paid_date

	LET orders_cnt = 1 
	FOREACH crs_orders INTO arr_orders[orders_cnt].* 
		LET orders_cnt = orders_cnt + 1 
		IF orders_cnt > 100 THEN
			EXIT FOREACH
		END IF
	END FOREACH 

	LET orders_cnt = orders_cnt -1
	CALL set_count(orders_cnt)
	INPUT ARRAY arr_orders WITHOUT DEFAULTS FROM sa_orders.*
		BEFORE ROW
			LET arrcurr = arr_curr()
			LET scrline = scr_line()
			DISPLAY arr_orders[arrcurr].* TO sa_orders[scrline].*
			ATTRIBUTE(reverse)
			
		ON KEY (control-f)
			LET arrcurr = arr_curr()
			CALL display_order_lines(arr_orders[arrcurr].order_num)
		AFTER ROW
			LET arrcurr = arr_curr()
			LET scrline = scr_line()
			DISPLAY arr_orders[arrcurr].* TO sa_orders[scrline].*
			ATTRIBUTE(normal)


	END INPUT

END FUNCTION # display_orders does not work

FUNCTION display_order_lines(p_order_num)
DEFINE p_order_num LIKE items.order_num
DEFINE idx INTEGER
DEFINE l_arr_items ARRAY[20] OF RECORD
	item_num LIKE items.item_num,
	stock_num LIKE items.stock_num,
	manu_code LIKE items.manu_code,
	quantity LIKE items.quantity,
	total_price LIKE items.total_price
END RECORD

DECLARE crs_items CURSOR FOR
	SELECT item_num,
	stock_num,
	manu_code,
	quantity,
	total_price
	FROM items
	WHERE items.order_num = p_order_num
	LET idx = 1
	FOREACH crs_items INTO l_arr_items[idx].*
		LET idx = idx + 1
	END FOREACH
	
	LET idx = idx - 1
	CALL set_count(idx)
	DISPLAY ARRAY l_arr_items TO sa_items.*
	close crs_items
	free crs_items
END FUNCTION
