 DATABASE stores7
 GLOBALS 
  DEFINE gr_stock RECORD LIKE stock.* 
  DEFINE nr_stock RECORD LIKE stock.* 
  DEFINE ga_stock ARRAY[10] OF RECORD LIKE stock.* 
  DEFINE ga_manufact ARRAY[50] OF RECORD 
     manu_code LIKE manufact.manu_code, 
     manu_name LIKE manufact.manu_name 
  END RECORD 
  DEFINE g_man_max SMALLINT 
  DEFINE g_man_loaded SMALLINT 
END GLOBALS 
