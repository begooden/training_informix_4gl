database stores_demo
DEFINE arr_stock_dyn DYNAMIC ARRAY OF RECORD LIKE stock.*
DEFINE arr_stockn ARRAY[100] OF RECORD LIKE stock.*

MAIN

	OPEN WINDOW w_stock2 AT 3,3 WITH FORM "stock_arr" 
	ATTRIBUTE ( BORDER ) 
	CALL dispar_stock_dyn()

END MAIN
FUNCTION dispar_stock_dyn () 
	DEFINE stock_cnt     SMALLINT, 
	i             SMALLINT, 
	cont_fetch    SMALLINT 
	DEFINE array_size SMALLINT

	DISPLAY "Press ACCEPT to continue, INTERRUPT to cancel." AT 1,1 
	DISPLAY "" AT 2,1 
	# Calculate array size
	SELECT count(*) INTO array_size
	FROM stock

	# Allocate array with this size
	ALLOCATE ARRAY arr_stock_dyn[array_size]

	DECLARE mstock_curs_dyn CURSOR FOR 
	SELECT stock.* 
	FROM stock 
	ORDER BY stock_num, manu_code 

	LET stock_cnt = 1 
	FOREACH mstock_curs_dyn INTO arr_stock_dyn[stock_cnt].* 
		DISPLAY arr_stock_dyn[stock_cnt].* TO sa_stock[stock_cnt].*
		LET stock_cnt = stock_cnt + 1 
		IF stock_cnt > 10 THEN
			ExIT FOREACH
		END IF
	END FOREACH 
END FUNCTION # dispar_stock_dyn does not work
