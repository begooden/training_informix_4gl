database stores_demo
DEFINE arr_manu ARRAY[10] OF RECORD
	manu_code LIKE manufact.manu_code,
	manu_name LIKE manufact.manu_name
END RECORD
DEFINE rep CHAR(1)
DEFINE num_elem INTEGER
MAIN
	DEFER INTERRUPT
	OPEN WINDOW my_windows at 2,2 WITH FORM "manu_wind"
	ATTRIBUTES (border)
	LET arr_manu[1].manu_code = "ANZ"
	LET arr_manu[1].manu_name = "Anzeeee"
	CALL set_count(5)
	INPUT ARRAY arr_manu 
	FROM sa_manufact.*
		BEFORE INPUT
			DISPLAY "Before INPUT" AT 2,2
			SLEEP 1
		AFTER INSERT
			DISPLAY "After insert" AT 2,2
			SLEEP 1
		AFTER ROW
			DISPLAY "After row" AT 2,2
			SLEEP 1
		AFTER FIELD manu_code
			DISPLAY "After field manu_code" AT 2,2
			SLEEP 1
		BEFORE INSERT
			DISPLAY "Before insert" AT 2,2
			SLEEP 1
		BEFORE ROW
			DISPLAY "Before row" AT 2,2
			SLEEP 1
		BEFORE FIELD manu_code
			DISPLAY "Before field manu_code" AT 2,2
			SLEEP 1
		AFTER INPUT
			LET num_elem = arr_count()
			DISPLAY "After INPUT" AT 2,2
			SLEEP 1
			IF (int_flag) THEN
				DISPLAY "Abandonné saisie ",num_elem
			ELSE
				DISPLAY "Validé saisie ",num_elem
			END IF
	END INPUT
	prompt "Fini ?" for char rep
END MAIN
