GLOBALS "stock_glob.4gl" 
DEFINE arr_stock_dyn DYNAMIC ARRAY OF RECORD LIKE stock.*

FUNCTION many_menu () 
	OPEN WINDOW w_stock2 AT 3,3 WITH FORM "stock_arr" 
	ATTRIBUTE ( BORDER ) 

	MENU "STOCK MANY" 
		COMMAND "Query" "Search for a group of stock entries" 
			CALL dispar_stock () 
		COMMAND "Add" "Add a new stock item" 
			CALL inpar_stock_ctl () 
		COMMAND "Exit" "Return to the STOCK MAIN menu" 
			EXIT MENU 
	END MENU 
	CLOSE WINDOW w_stock2 
END FUNCTION   # many_menu

FUNCTION inpar_stock_ctl () 
	DEFINE i SMALLINT 
	DISPLAY "Press ACCEPT to ADD, INTERRUPT to cancel." AT 1,1 
	DISPLAY "Press CTRL-E to erase row, CTRL-N for next row, ", "CTRL-P for previous row." AT 2,1 

	CALL stock_arr_init () 
	CLEAR FORM 
	LET int_flag = FALSE 
	INPUT ARRAY ga_stock FROM sa_stock.* 

	IF ( int_flag ) THEN 
		LET int_flag = FALSE 
		ERROR "Array entry canceled" 
	ELSE 
		CALL ins_arr () 
	END IF 

END FUNCTION  # inpar_stock_ctl

FUNCTION stock_arr_init () 
	DEFINE i   SMALLINT 
	INITIALIZE ga_stock[1].* TO NULL 
	FOR i = 2 TO 5 
		LET ga_stock[i].* = ga_stock[1].* 
	END FOR 
END FUNCTION # stock_arr_init

FUNCTION ins_arr () 
	DEFINE i    SMALLINT 

	FOR i = 1 TO ARR_COUNT() 
		INSERT INTO stock 
		VALUES ( ga_stock[i].* ) 
	END FOR 
	LET i = i - 1 

	MESSAGE "There were ", i USING "<<", " rows added." 

END FUNCTION  # ins_arr

FUNCTION dispar_stock () 
	DEFINE stock_cnt     SMALLINT, 
	i             SMALLINT, 
	cont_fetch    SMALLINT 

	DISPLAY "Press ACCEPT to continue, INTERRUPT to cancel." AT 1,1 
	DISPLAY "" AT 2,1 

	DECLARE mstock_curs CURSOR FOR 
	SELECT stock.* 
	FROM stock 
	ORDER BY stock_num, manu_code 

	OPEN mstock_curs 

	LET cont_fetch = TRUE 

	WHILE ( SQLCA.SQLCODE = 0 ) AND ( cont_fetch ) 
		LET stock_cnt = 0 

		WHILE ( SQLCA.SQLCODE = 0 ) AND ( stock_cnt < 5 ) 
			LET stock_cnt = stock_cnt + 1 
			FETCH mstock_curs INTO ga_stock[stock_cnt].* 
			IF ( SQLCA.SQLCODE = NOTFOUND ) THEN 
				LET stock_cnt = stock_cnt - 1    
			END IF 
		END WHILE 

		IF ( stock_cnt != 0 ) THEN 

			CALL show_stock ( stock_cnt ) 
			RETURNING cont_fetch 
		ELSE 
			LET cont_fetch = FALSE 
		END IF 
		CALL stock_arr_init () 

	END WHILE 
END FUNCTION # dispar_stock

FUNCTION show_stock ( p_stock_cnt ) 

DEFINE p_stock_cnt   SMALLINT
DEFINE cont_disp_arr SMALLINT 

LET int_flag = FALSE 
LET cont_disp_arr = TRUE 
CALL SET_COUNT ( p_stock_cnt ) 
DISPLAY ARRAY ga_stock TO sa_stock.* 
CLEAR FORM 

IF ( int_flag ) THEN 
	LET int_flag = FALSE 
	LET cont_disp_arr = FALSE 
END IF 

RETURN cont_disp_arr 
END FUNCTION 

FUNCTION dispar_stock_dyn () 
	DEFINE stock_cnt     SMALLINT, 
	i             SMALLINT, 
	cont_fetch    SMALLINT 
	DEFINE array_size SMALLINT

	DISPLAY "Press ACCEPT to continue, INTERRUPT to cancel." AT 1,1 
	DISPLAY "" AT 2,1 
	# Calculate array size
	SELECT count(*) INTO array_size
	FROM stock

	# Allocate array with this size
	ALLOCATE ARRAY arr_stock_dyn[array_size]

	DECLARE mstock_curs_dyn CURSOR FOR 
	SELECT stock.* 
	FROM stock 
	ORDER BY stock_num, manu_code 

	LET stock_cnt = 1 
	FOREACH mstock_curs_dyn INTO arr_stock_dyn[stock_cnt].* 
		DISPLAY arr_stock_dyn[stock_cnt].* TO sa_stock[stock_cnt].*
		LET stock_cnt = stock_cnt + 1 
	END FOREACH 
END FUNCTION # dispar_stock_dyn does not work
