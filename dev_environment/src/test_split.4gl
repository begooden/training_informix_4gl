DEFINE  val1,val2,val3,val4 , in_string char(320)
DEFINE delimiter char(10)
DEFINE stringnum ,strict, s_status integer

MAIN
PROMPT "Input a string with numbers separated by a , up to 4 " FOR in_string
	LET delimiter = ","
	LET stringnum = 4
	LET strict = 0
    CALL split_string(in_string,delimiter,stringnum,strict) RETURNING s_status,val1,val2,val3,val4
	display "val1= ",val1 clipped
	display "val2= ",val2 clipped
	display "val3= ",val3 clipped
	display "val4= ",val4 clipped
END MAIN
