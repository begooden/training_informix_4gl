 GLOBALS "stock_glob.4gl" 

 FUNCTION updel_init() 
DEFINE prepvar CHAR(250) 
LET prepvar = "DELETE FROM stock ", 

     "WHERE CURRENT OF lockstock" 

  PREPARE del_stock FROM prepvar 

  LET prepvar = "UPDATE stock ", 
     "SET(description, unit_price, unit, unit_descr) =", 
     "(?,?,?,?) ", 
     "WHERE CURRENT OF lockstock" 

  PREPARE upd_stock FROM prepvar 
  DECLARE lockstock CURSOR FOR 

     SELECT * FROM stock 
     WHERE stock.stock_num = gr_stock.stock_num AND 
           stock.manu_code = gr_stock.manu_code 
     FOR UPDATE 
END FUNCTION 

FUNCTION check_items() 
  DEFINE stock_count SMALLINT, 
          del_ok SMALLINT 

  LET del_ok = TRUE 

     SELECT COUNT(*) INTO stock_count FROM items 
       WHERE items.stock_num = gr_stock.stock_num AND 
           items.manu_code = gr_stock.manu_code 

  IF stock_count > 0 THEN 
     ERROR "This stock item is on order. It cannot be deleted." 

     LET del_ok = FALSE 
  END IF 

  RETURN del_ok 

END FUNCTION 


FUNCTION del_menu() 

  DEFINE del_ok SMALLINT 

  MENU "ARE YOU SURE?" 
     COMMAND "No" "Do not delete this stock item." 
       LET del_ok = FALSE 
       ERROR "Delete cancelled." 
       EXIT MENU 

     COMMAND "Yes" "Delete this stock item." 
       LET del_ok = TRUE 
       EXIT MENU 
  END MENU 
  RETURN del_ok 

END FUNCTION 

FUNCTION lock_stock() 

  DEFINE lock_ok SMALLINT 
  LET lock_ok = TRUE 

  WHENEVER ERROR CONTINUE 
  OPEN lockstock 
  FETCH lockstock INTO gr_stock.* 
  WHENEVER ERROR STOP 

  IF SQLCA.SQLCODE = NOTFOUND THEN 
     ERROR "This row has already been deleted." 
     LET lock_ok = FALSE 
  ELSE 
     IF SQLCA.SQLCODE < 0 THEN 
          ERROR "Row could not be locked." 
          LET lock_ok = FALSE 
     END IF 
  END IF 
  RETURN lock_ok 
END FUNCTION 

FUNCTION delete_stock() 

DEFINE del_ok SMALLINT 

  CALL check_items() RETURNING del_ok 

  IF del_ok THEN 
     CALL del_menu() RETURNING del_ok 
  END IF 

  IF del_ok THEN 
       BEGIN WORK 
     CALL lock_stock() RETURNING del_ok 

     IF del_ok THEN 
      WHENEVER ERROR CONTINUE 
      EXECUTE del_stock 
      WHENEVER ERROR STOP 

      IF SQLCA.SQLCODE != 0 THEN 
         ERROR "Error number ", SQLCA.SQLCODE USING "-<<<<" , " has occurred." 
         LET del_ok = FALSE 
      END IF 
    END IF 

    IF del_ok THEN 
         COMMIT WORK 

      MESSAGE "Stock item deleted." 
    ELSE 
         ROLLBACK WORK 
    END IF 
 END IF 
END FUNCTION 

FUNCTION update_stock() 

 DEFINE ur_stock RECORD LIKE stock.*, 
         upd_ok SMALLINT 


 DISPLAY "Press ACCEPT after you have updated ", "the stock information.", "" AT 1,1 
 DISPLAY "Press INTERRUPT to cancel.","" AT 2,1 

 LET ur_stock.* = gr_stock.* 
    BEGIN WORK 
 CALL lock_stock() RETURNING upd_ok 

 IF upd_ok THEN 
    CALL inp_upd_stock() RETURNING upd_ok 
 END IF 

 IF upd_ok THEN 
      WHENEVER ERROR CONTINUE 
      EXECUTE upd_stock USING gr_stock.description THRU gr_stock.unit_descr 
      WHENEVER ERROR STOP 

      IF SQLCA.SQLCODE < 0 THEN 
         ERROR "Error number ", 
         SQLCA.SQLCODE USING "-<<<<"," has occurred." 
         LET upd_ok = FALSE 
      END IF 
 END IF 

 IF upd_ok THEN 
      COMMIT WORK 
    MESSAGE "Stock Updated" 
 ELSE 
      ROLLBACK WORK 
    LET gr_stock.* = ur_stock.* 
    CALL display_stock() 
 END IF 
END FUNCTION 

FUNCTION inp_upd_stock() 

 DEFINE upd_ok SMALLINT 
 LET upd_ok = TRUE 
 LET int_flag = FALSE 

 INPUT BY NAME gr_stock.description, 
    gr_stock.unit_price, 
    gr_stock.unit, 
    gr_stock.unit_descr 
WITHOUT DEFAULTS 

 IF int_flag THEN 
    LET int_flag = FALSE 
    ERROR "Update cancelled." 
    LET upd_ok = FALSE 
 END IF 

 RETURN upd_ok 
END FUNCTION 
