
GLOBALS "stock_glob.4gl" 

FUNCTION query_stock_ctl () 
	DEFINE where_clause   CHAR(300)
	DEFINE query_ok       SMALLINT
	DEFINE stock_cnt      SMALLINT 

	CALL stock_const () 
	RETURNING query_ok, where_clause 

	IF ( query_ok ) THEN 
		CALL query_stock ( where_clause ) 
		RETURNING query_ok 
		IF ( query_ok ) THEN 
			CALL get_stock_cnt ( where_clause ) 
			RETURNING stock_cnt 
			MESSAGE "There were ", stock_cnt USING "<<<<"," rows "
		END IF 
	END IF 
	RETURN query_ok 
END FUNCTION # query_stock_ctl

FUNCTION stock_const () 
	DEFINE where_clause   CHAR(300)
	DEFINE const_ok       SMALLINT 

	DISPLAY "Enter search criteria, press ACCEPT to Query.", "" AT 1,1 
	DISPLAY "" AT 2,1 
	LET const_ok = TRUE 
	LET int_flag = FALSE 

	CONSTRUCT BY NAME where_clause ON stock.* 

	IF ( int_flag ) THEN 
		LET int_flag = FALSE 
		CLEAR FORM 
		LET const_ok = FALSE 
	END IF 

	RETURN const_ok, where_clause 

END FUNCTION  # stock_const

FUNCTION query_stock ( p_where_clause ) 
	DEFINE p_where_clause   CHAR(300)
	DEFINE sql_text         CHAR(300)
	DEFINE fetch_ok         SMALLINT 

	LET sql_text = "SELECT stock.stock_num, stock.manu_code ", 
	" FROM stock ", 
	"WHERE ", p_where_clause CLIPPED 

	PREPARE stock_stmt FROM sql_text 
	DECLARE stock_curs SCROLL CURSOR WITH HOLD FOR stock_stmt 

	OPEN stock_curs 
	CALL fetch_stock ( 1 ) RETURNING fetch_ok 

	IF ( fetch_ok ) THEN 
		CALL display_stock () 
	ELSE 
		ERROR "No rows found." 
	END IF 
	RETURN fetch_ok 
END FUNCTION  # query_stock

FUNCTION get_stock_cnt ( p_where_clause ) 
	DEFINE p_where_clause   CHAR(300)
	DEFINE sql_text         CHAR(300)
	DEFINE stock_cnt        SMALLINT 

	LET sql_text = "SELECT COUNT(*) ", 
	" FROM stock ", 
	"WHERE ", p_where_clause CLIPPED 

	PREPARE stock_cnt_stmt FROM sql_text 
	DECLARE stock_cnt_curs CURSOR FOR stock_cnt_stmt 
	OPEN stock_cnt_curs 
	FETCH stock_cnt_curs INTO stock_cnt 
	CLOSE stock_cnt_curs 
	FREE stock_cnt_stmt 
	RETURN stock_cnt 

END FUNCTION # get_stock_cnt

FUNCTION fetch_rel_stock ( p_fetch_flag ) 
	DEFINE p_fetch_flag     SMALLINT
	DEFINE fetch_ok         SMALLINT 

	CALL fetch_stock ( p_fetch_flag ) 
	RETURNING fetch_ok 

	IF ( fetch_ok ) THEN 
		CALL display_stock ( ) 
	ELSE 
		IF ( p_fetch_flag = 1 ) THEN 
			ERROR "You are at the end of the list" 
		ELSE 
			ERROR "You are at the beginning of the list" 
		END IF 
	END IF 
END FUNCTION  # fetch_rel_stock

FUNCTION fetch_stock ( p_fetch_flag ) 
	DEFINE p_fetch_flag     SMALLINT
	DEFINE fetch_ok         SMALLINT 

	LET fetch_ok = TRUE 
	FETCH RELATIVE p_fetch_flag stock_curs 
	INTO gr_stock.stock_num, gr_stock.manu_code 

	IF ( SQLCA.SQLCODE = NOTFOUND ) THEN 
		LET fetch_ok = FALSE 
	ELSE 
		CALL fetch_whole_row () 
	END IF 

	RETURN fetch_ok 

END FUNCTION # fetch_stock

FUNCTION display_stock () 
	DISPLAY BY NAME gr_stock.* 
END FUNCTION # display_stock

FUNCTION clean_up () 
	WHENEVER ERROR CONTINUE 
	CLOSE stock_curs 
	FREE stock_curs 
	FREE stock_stmt 
	WHENEVER ERROR STOP 
END FUNCTION # clean_up

FUNCTION stock_qry_init ( ) 
	DEFINE sql_text    CHAR(300) 
	LET sql_text = "SELECT stock.* ", 
	" FROM stock ", 
	" WHERE stock.stock_num = ? ", 
	" AND stock.manu_code = ? " 

	PREPARE stock_all_stmt FROM sql_text 
	DECLARE stock_all_curs CURSOR FOR stock_all_stmt 
END FUNCTION # stock_qry_init

FUNCTION fetch_whole_row () 
	OPEN stock_all_curs USING gr_stock.stock_num, gr_stock.manu_code 
	FETCH stock_all_curs INTO gr_stock.* 
END FUNCTION  # fetch_whole_row
