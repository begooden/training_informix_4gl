 GLOBALS "stock_glob.4gl" 

 MAIN 

   DEFER INTERRUPT 

   CALL set_options() 

   CALL main_init() 

  CALL updel_init() 

  CALL stock_qry_init() 

  CALL main_menu() 

  CLEAR SCREEN 

END MAIN 


FUNCTION main_init() 
  CALL startlog("stock_errlog") 
  LET g_man_max = 50 

  INITIALIZE nr_stock.* TO NULL 
  LET gr_stock.* = nr_stock.* 
  OPEN FORM f_stock FROM "stock" 

  DECLARE manu_ptr CURSOR FOR 
     SELECT manu_code, manu_name 
     FROM manufact 
     ORDER BY manu_code 

  LET g_man_loaded= 1 

  FOREACH manu_ptr INTO ga_manufact[g_man_loaded].* 
     LET g_man_loaded = g_man_loaded + 1 
     IF g_man_loaded > g_man_max THEN 
       EXIT FOREACH 
     END IF 
  END FOREACH 
  FREE manu_ptr 
  LET g_man_loaded = g_man_loaded - 1 
END FUNCTION 

FUNCTION set_options() 

  OPTIONS 
     MESSAGE LINE LAST - 1, 
     HELP FILE "stock_help.ex", 
     HELP KEY CONTROL-B, 
     INPUT WRAP 
END FUNCTION 


FUNCTION main_menu() 

  MESSAGE "Type the first letter of the option you want, ", "or CONTROL-B for help." 

  MENU "STOCK MAIN" 

     COMMAND "One-Stock" "Go to the Single Row maintenance menu." 
       HELP 101 
       CALL one_menu() 

     COMMAND "Multiple-Stock" "Go to the Multiple Row maintenance menu." 
       HELP 102 
       CLEAR SCREEN 
       CALL many_menu() 

     COMMAND "Report" "Go to the Report menu." 
       HELP 103 
       CALL dummy() 

     COMMAND "Exit" "Return to the Operating System." 
       HELP 901 
       EXIT MENU 
  END MENU 

END FUNCTION 


FUNCTION dummy() 

  ERROR "Function not yet implemented." 

END FUNCTION 
