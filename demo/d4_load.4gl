database stores7
main

  load from "stock.unl" delimiter "|" insert into stock;
  load from "items.unl" delimiter "|" insert into items;
  load from "manufact.unl" delimiter "|" insert into manufact;
  load from "orders.unl" delimiter "|" insert into orders;
  load from "customer.unl" delimiter "|" insert into customer;
  load from "state.unl" delimiter "|" insert into state;
  load from "syscolatt.unl" delimiter "|" insert into syscolatt;

end main
