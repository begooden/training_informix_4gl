#########################################################################
# Makefile for programm :  nom du programme
#########################################################################
# syntaxe pour executer le make:
# make -f stock.make informix               ou bien
# make -f M74.4ge.make clean		ou bien	
# Modele developpe par eric@begooden.it
#########################################################################

# include des definitions generiques
# NE PAS SUPPRIMER LA LIGNE SUIVANTE !
include ../compile.defs

#################################################################################################################
#### localisations par defaut ( definies dans .defs )
# le 4ge est garde dans $BIN_DIR
# les .o sont gardes dans $BUILD_4GL
# les formes sont gardees dans $FRM
# les .o de C sont gardes dans $CLIB
#################################################################################################################

######### Vous pouvez modifier le fichier a partir de ce point   #####################################################
# definition des Targets specifiques a ce Makefile
# nom de l executable
4GE = $(BIN_DIR)/stock.4ge

# definition des modules de globals
GLOBALS_4GL_O= $(OBJ_4GL_DIR)/stock_glob.o

# definition des sources principaux du programme
OBJETS_4GL_O = 	$(OBJ_4GL_DIR)/stock_arr.o \
	$(OBJ_4GL_DIR)/stock_brws.o \
	$(OBJ_4GL_DIR)/stock_chng.o \
	$(OBJ_4GL_DIR)/stock_main.o \
	$(OBJ_4GL_DIR)/stock_one.o

# definition des objets 4gl communs du programme
COMMON_4GL_O=

# definition des librairies 4gl utilisees
LIB_4GL_A=

# definition des objets C
OBJETS_C=

# definition des objets ESQL/C
OBJETS_ESQLC=

IFMX_FORMS = $(FORMS_DIR)/manu_wind.frm \
	$(FORMS_DIR)/stock_arr.frm \
	$(FORMS_DIR)/stock.frm

# include des regles generiques de compilation. Ces regles peuvent etre redefinies, si necessaire, apres la ligne d include
# NE PAS SUPPRIMER LA LIGNE SUIVANTE !
include ../compile.rules

# les targets all, informix et lycia sont definis dans compile.rules, il peuvent cependant etre redefinis ci-dessous
# informix = compile .4gl->o et .per->.frm, produit un .4ge
# rds = compile .4gl->4go et .per->.frm, produit un .4gi
# lycia = compile .4gl->4o et .per->.fm2, produit un .exe
