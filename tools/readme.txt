Purpose:
fgledit.pl is a simple perl script that "replicates" part of the behaviour
or r4gl and i4gl development cycle, ie edit the source, compile it, get errors (sometimes :-),
edit the source with errors, positions the cursor on the first error occurrence,
cleans the errors text lines once the file is edited to have a brand new clean source file.

This utility is great when you compile with makefiles, and avoids you to edit the .err file, believing
you have edited the .4gl or .per file...

You can also get a backup file ( timestamped+login name ), using the -k option, or by forcing $KeepBackup=1
in the code.

It works with .4gl files and .per files.

For now it works in unix environment and vi, and also windows and notepad++ . It can be adapted to other editors once
you set the right command line to get to the first error occurrence using its line number.

Installation:
* unix: copy this file somewhere where you have a valid $PATH. Ensure you have chmod 755 fgledit.pl'ed
* windows: ensure you have perl on board, copy the file somewhere where you have a valid $PATH.

Requirements:
You need perl on the system you want to use fgledit.pl.
Please ensure to have /tmp directory existing in unix environment ( more than likely ), 
or c:\temp in windows environment ( not so likely ).

Syntax:
fgledit.pl <filename>
or 
fgledit.pl <source name> -k
The -k option will make a backup copy of your source file into the $TempDir directory

Examples: 
fgledit.pl my4glmodule
	will edit my4glmodule.4gl, or my4glmodule.err if existing.

fgledit.pl my4glmodule -k
	will do the same thing, and will store a backup file in $TempDir. The backup file name is displayed after editing the source file.

fgledit myform
	will edit myform.per, or myform.err if existing.

Comments:
you do not need to specify the extension of your file (.4gl or .per). Nevertheless, you can do it if you really want it :-)
If the .err file exists, it will take this one, else it will take the source .4gl or .per file

If you want the backup file somewhere else than /tmp or c:\TEMP, feel free to export the TEMPDIR environment
variable to any exist directory.

On windows, you can either run the utility from a command line window, or double click on it in the explorer. In this case, you will
be asked to enter the file name.

Please do not hesitate to contact me if you find "unexpected behaviours": this may happen, although I use
this utility for months now.

my contact is: eric.vercelletto@begooden-it.com

Have fun!
Eric
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
