# compile.rules: determine les regles generiques de compilation pour informix 4GL compile, fourjs et i4glormix rds
######################################################################################################################

# Macros RDS
4GI := $(4GE:.4ge=.4gi)
OBJETS_4GL_4GO := $(OBJETS_4GL_O:.o=.4go)
GLOBALS_4GL_4GO := $(GLOBALS_4GL_O:.o=.4go)
COMMON_4GL_4GO := $(COMMON_4GL_O:.o=.4go)
LIB_4GL_4GO := $(LIB_4GL_A:.a=.4go)

# Macros Lycia
EXE := $(4GE:.4ge=.exe)
OBJETS_4GL_4O := $(OBJETS_4GL_O:.o=.4o)
GLOBALS_4GL_4O := $(GLOBALS:.o=.4o)
COMMON_4GL_4O := $(COMMON_4GL_O:.o=.4o)
LIB_4GL_40 := $(LIB_4GL_A:.o=.4o)

# targets generiques
4ge: $(4GE)
4gi: $(4GI)
exe: $(EXE)
forms: $(IFMX_FORMS)
lib: $(LIB)
4geprog:	4ge forms
4giprog:  4gi forms
lyciaprog: exe forms
rep :
	@if [ ! -d $(OBJ_4GL_DIR) ]; then mkdir -p $(OBJ_4GL_DIR); fi
	@cat /dev/null > $(LOGFILE)

reset:	
	@echo -n "$(TS) Cleaning all executables,objects and forms " | tee -a $(LOGFILE)
	@rm -f $(OBJETS_4GL_O);rm -f $(OBJETS_4GL_4GO);rm -f $(IFMX_FORMS);rm $(4GE);rm $(4GI)

clean_o:
	@rm -f $(OBJETS_4GL_O)

clean_4go:
	@rm -f $(OBJETS_4GL_4GO)

clean_forms:
	@rm -f $(IFMX_FORMS)

copyobj:
	@cp $(OBJETS_4GL_O) $(GLOBALS_4GL_O) $(OBJ_4GL_DIR)
#
######################################################################################################################
## Definition des regles de compilation
#
# 1ere partie: regles generales pour informix 4gl compile
$(4GE) :	$(GLOBALS_4GL_O) $(OBJETS_4GL_O) $(COMMON_4GL_O) $(LIB_4GL_A) $(OBJETS_C)
	@echo -n "$(TS) Linking Standard Informix Program $@ " | tee -a $(LOGFILE)
	@c4gl -o $(4GE)  $(GLOBALS_4GL_O) $(OBJETS_4GL_O) $(COMMON_4GL_O) $(LIB_4GL_A) $(OBJETS_C) ; if [ $$? -eq 0 ]; then echo "OK :-)" | tee -a $(LOGFILE); else echo "FAILED :-(" | tee -a $(LOGFILE) ; \
	if [ $(STOPLINK) -eq 1 ]; then exit 1 ; fi;fi;

$(OBJ_4GL_DIR)/%.o: %.4gl
	@echo -n "$(TS) Building $*.o " | tee -a $(LOGFILE)
	@c4gl -c $? ; if [ $$? -eq 0 ]; then mv $*.o $@; echo "OK :-)" | tee -a $(LOGFILE); else  echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

$(LIB) : $(OBJETS_LIB)
	@echo -n "$(TS) Building 4gl library $@ " | tee -a $(LOGFILE)
	ar cr $(LIB) $(OBJETS_LIB)	; if [ $$? -eq 0 ]; then echo "OK :-)" | tee -a $(LOGFILE) ; else echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

######################################################################################################################
# 2eme partie: regle pour informix rds
$(4GI) :	$(GLOBALS_4GL_4GO) $(OBJETS_4GL_4GO) $(COMMON_4GL_4GO) $(LIB_4GL_4GO) 
	@echo -n "$(TS) Building RDS Program $@ " | tee -a $(LOGFILE)
	@cat $(GLOBALS_4GL_4GO) $(OBJETS_4GL_4GO) $(COMMON_4GL_4GO) $(LIB_4GL_4GO) > $@ ;if [ $$? -eq 0 ]; then echo "OK :-)" | tee -a $(LOGFILE); else echo "FAILED :-(" | tee -a $(LOGFILE) ; \
	        if [ $(STOPLINK) -eq 1 ]; then exit 1 ; fi;fi;

$(OBJ_4GL_DIR)/%.4go: %.4gl
	@echo -n "$(TS) Building $*.4go " | tee -a $(LOGFILE)
	@fglpc $? ; if [ $$? -eq 0 ]; then mv $*.4go $@; echo "OK :-)" | tee -a $(LOGFILE); else  echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

######################################################################################################################
# 3eme partie: regles pour compiler les formes 4gl compile ou rds
$(FORMS_DIR)/%.frm: $(FORMS_DIR)/%.per
	@echo -n "$(TS) Building informix form $*.frm " | tee -a $(LOGFILE)
	@form4gl $? 1>/dev/null; if [ $$? -eq 0 ]; then a=1; echo "OK :-)" | tee -a $(LOGFILE); else  echo "FAILED :-(" | tee -a $(LOGFILE) ; a=1 ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

%.frm: %.per
	echo -n "$(TS) Building informix form $*.frm " | tee -a $(LOGFILE)
	form4gl $? 1>/dev/null; if [ $$? -eq 0 ]; then a=1; echo "OK :-)" | tee -a $(LOGFILE); else  echo "FAILED :-(" | tee -a $(LOGFILE) ; a=1 ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

######################################################################################################################
#4eme partie: regles pour compiler les C 
$(OBJETS_C_DIR)/%.o: %.c
	@gcc -c $? ; if [ $$? -eq 0 ]; then mv $*.o $@; echo "OK :-)" | tee -a $(LOGFILE); else  echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ 
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi
#
######################################################################################################################

# 5eme partie: regles pour compiler les esql C
$(OBJETS_ESQLC_DIR)/%.o: %.ec
	esql -c $? ; if [ $$? -eq 0 ]; then mv $*.o $@; echo "OK :-)" | tee -a $(LOGFILE); else  echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ 
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

######################################################################################################################
# 6eme partie: regles generales pour  Lycia compile

OBJETS_4GL_4O := $(OBJETS_4GL_O:.o=.4o)
GLOBALS_4GL_4O := $(GLOBALS:.o=.4o)
COMMON_4GL_4O := $(COMMON:.o=.4o)
LIB_4GL_40 := $(LIB:.o=.4o)


$(EXE) :	$(GLOBALS_4GL_4O) $(OBJETS_4GL_4O) $(COMMON_4GL_4O) $(LIB_4GL_40)
	@echo -n "$(TS) Linking Lycia Program $@ " | tee -a $(LOGFILE)
	@qlink -o $(EXE)  $(GLOBALS_4GL_4O) $(OBJETS_4GL_4O) $(COMMON_4GL_4O) $(LIB_4GL_40) $(OBJETS_C) ; if [ $$? -eq 0 ]; then echo "OK :-)" | tee -a $(LOGFILE); else echo "FAILED :-(" | tee -a $(LOGFILE) ; \
	if [ $(STOPLINK) -eq 1 ]; then exit 1 ; fi;fi;

$(BUILD)/%.4o: $(SRC)/%.4gl
	@echo -n "$(TS) Building $*.4o " | tee -a $(LOGFILE)
	@qfgl -c $? -o $@; if [ $$? -eq 0 ]; then mv $*.4o $@; echo "OK :-)" | tee -a $(LOGFILE); else  echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

%.4o: %.4gl
	@echo -n "$(TS) Building $*.4o " | tee -a $(LOGFILE)
	@qfgl -c $? -o $@ ; if [ $$? -eq 0 ]; then echo "OK :-)" | tee -a $(LOGFILE) ; else echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

$(COMPIL)/%.4o: $(SRC)/%.4gl
	@echo -n "$(TS) Building $*.4o " | tee -a $(LOGFILE)
	@qfgl -c $? -o $@ ; if [ $$? -eq 0 ]; then echo "OK :-)" | tee -a $(LOGFILE) ; else echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

$(LIB_4GL_40) : $(OBJETS_LIB)
	@echo -n "$(TS) Building 4gl library $@ " | tee -a $(LOGFILE)
	ar cr $(LIB) $(OBJETS_LIB)	; if [ $$? -eq 0 ]; then echo "OK :-)" | tee -a $(LOGFILE) ; else echo "FAILED :-(" | tee -a $(LOGFILE) ; rm $@ ; \
	if [ $(STOPCOMPIL) -eq 1 ]; then exit 2 ; fi; fi

######################################################################################################################
