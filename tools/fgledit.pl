# Author : Eric Vercelletto
# (C) 2006-2010 BeGooden-IT Consulting
# http://www.kandooerp.org
# $Id: $
# $Revision: $: last commit revision number
# $Author: $: last commit author
# $Date: $: last commit date
# $Message: $
# Description  : replicates behaviour of r4gl or i4gl environment, ie vi's  the .err file, reaching 1st error line
# and saves the .4gl file, else vi's directly the .4gl
# it works same way for .per
# can keep a copy of original file using the -k option

use File::Copy;
use File::stat;
my $ArgC = @ARGV ;

# please set KeepBackup to 1 if you want to always keey a pre-backup of your file
my $KeepBackup=0 ;

# please select your preferred editor here, determining the right command line to position cursor on the first error occurrence
if ( $^O  =~ /linux|aix|hp-ux|solaris|sco/i ) {
	$EditorCmd="vi -c " ;
} elsif ( $^O =~ /MSWin/ ) {
	$EditorCmd="\"C:\\Program Files\\Notepad++\\notepad++.exe\" -n" ;
}

#
my $Pid=$$;
my $login = getlogin ;
my $Timestamp=time_to_date(time,"yymd_hms") ;
my $Extension=sprintf "%s\.%s",${Timestamp},${login} ;
if ( defined($ENV{"TEMPDIR"})) {
	$TempDir=$ENV{"TEMPDIR"} ;
} else {
	if ( $^O =~ /MSWin/ ) {
		$TempDir="c:/temp" ;
	} else {
		$TempDir="/tmp" ;
	}
	if ( ! -d ($TempDir) ) {
		die "Temp Directory does not exist: " . $TempDir ;
	}
}

if ( $ArgC < 1  ) {
	printf "Please enter the name of the file you want to edit " ;
	$SourceName=<STDIN> ;
	chomp ( $SourceName ) ;
	die "Please enter a valid source name" if (length($SourceName) == 0 ) ;
} else {
	$SourceName=$ARGV[0];
}

$SourceBaseName = $SourceName;
$SourceBaseName =~ s/\.\w*$// ;

# Superceeds hardcode $KeepBackup
if ( defined( $ARGV[1] && $ARGV[1] =~ /k/i ) ) {
	$KeepBackup=1 ;
}

# identify whether this is a .4gl or .per
$SourceFileName=sprintf "%s\.4gl",${SourceBaseName} ;
if ( ! ( -e $SourceFileName ) ) {
	$SourceFileName=sprintf "%s\.per",${SourceBaseName} ;
	$ErrLocateString="\^\\#";
} else {
	$ErrLocateString="\^\\|" ;
}

$ErrorFileName=sprintf "%s\.err",${SourceBaseName};
$BackupFileName=sprintf "%s/%s.%s",$TempDir,${SourceFileName},$Extension;

# Backup the original 4gl or per file
copy ($SourceFileName,$BackupFileName) ;

# set default position in file to line # 1
$ErrorLine=1;
if ( -w $ErrorFileName ) {
	$stats_source=stat($SourceFileName) ;
	$stats_error=stat($ErrorFileName) ;
	$ErrorLine=FindErrorLine($ErrorFileName,$ErrLocateString) ;
	$cmd=sprintf "%s%d %s",$EditorCmd,$ErrorLine,$ErrorFileName ;
	$status=system ( $cmd) ;
	if ( $status == 0 ) {
		open ERRORFILE,$ErrorFileName or die "Could not open Error File " . $ErrorFileName;
		@ErrFile=<ERRORFILE> ;
		if ( $SourceFileName =~ /\.4gl/ ) {
			@CleanFile = grep !/^\|/,@ErrFile;
		} elsif ( $SourceFileName =~ /\.per/ ) {
			@CleanFile = grep !/^\#/,@ErrFile;
		}
		open SOURCEFILE,">",$SourceFileName  or die "Could not open source File " . $SourceFileName;
		print SOURCEFILE @CleanFile;
		if ( $? == 0 ) {
			close SOURCEFILE ;
			close ERRORFILE ;
			unlink ($ErrorFileName) or print "Unable to delete $ErrorFileName. Reason $!\n"; 
			if ( !($KeepBackup) ) {
				unlink ($BackupFileName) or print "Unable to delete $BackupFileName. Reason $!\n";
			} else {
				printf "Backup file %s is available\n",$BackupFileName ;
			}
		} else {
			printf "Error while saving %s: restoring backup (to be fixed again)\n",$SourceFile;
			copy ($BackupFileName,$SourceFileName) ;
		}
	}
} else {
	if ( -w $SourceFileName ) {
		$cmd=sprintf "%s 1 %s",$EditorCmd,$SourceFileName ;
		$status=system ( $cmd) ;
		if ( !($KeepBackup) ) {
			unlink ($BackupFileName) or print "Unable to delete $BackupFileName. Reason $!\n";
		} else {
			printf "Backup file %s is available\n",$BackupFileName ;
		}
	} else {
		die "$SourceFileName does not exist or cannot be modified, please check" ;
	}
}

sub time_to_date {
my $TS_seconds=$_[0];
($S, $M, $H, $d, $m, $y, $wd, $aj, $isdst) = localtime $TS_seconds;
$year = $y + 1900;
$day=$d;
$month=$m + 1;
$TS_DSF=sprintf "%4s%02d%02d_%02d%02d%02d",$year, $month, $day, $H,$M,$S ;
return $TS_DSF ;
}

sub FindErrorLine {
my $ErrorFile=$_[0] ;
my $ErrLocateString=$_[1] ;
open ERRFILE,$ErrorFile or die "Could not open Error File " . $ErrorFile;
my $line="";
my $linenum=0 ;
while ( $line=<ERRFILE> ) {
	if ( $line =~ /$ErrLocateString/ ) {
		close ERRFILE ;
		return ($linenum+1) ;
	}
	$linenum++;
}
close ERRFILE ;
return ($$linenum+1) ;
} # end sub FindErrorLine
